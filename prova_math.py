# aritmetica.py
def add(a, b):
    return a + b


def sub(a, b):
    return a - b


def mul(a, b):
    return a * b


def div(a, b):
    return a / b


if __name__ == '__main__':
    print("__name__ = {} ".format(__name__))
    import sys  # importiamo il modulo sys della libreria standard
    # definiamo un dict di operazioni che mappa i nomi con le funzioni corrispondenti
    flag = True
    while flag:
        ops = dict(add=add, sub=sub, mul=mul, div=div)
        # chiediamo all'utente di scegliere l'operazione
        choice = input("Seleziona un'operazione [add/sub/mul/div/exit]: ")
        if choice in ops:
            # se la scelta non è valida terminiamo il programma con un messaggio d'errore
            print("ok choiche")
            # assegnamo a op la funzione scelta dall'utente
            op = ops[choice]
            try:
                # chiediamo all'utente di inserire i due valori, e proviamo a convertirli in float
                a = float(input('Inserisci il primo valore: '))
                b = float(input('Inserisci il secondo valore: '))
                print('Il risultato è:', op(a, b))
            except ZeroDivisionError:
                print('Errore valori errati')
        elif choice == "exit":
            sys.exit("Uscita programma")
            error = False
        elif choice not in ops:
            print('Scelta non corretta')
else:
    print("no main : {}".format(__name__))
