class Dog(object):

    # def __init__(self, name):
    #    self.name = name

    def set_name(self, name):
        self.name = name

    def set_eta(self, eta):
        self.eta = eta

    def set_razza(self, razza):
        self.razza = razza

    def set_sex(self, sex):
        self.sex = sex

    def get_name(self):
        return self.name

    def get_eta(self):
        return self.eta

    def get_razza(self):
        return self.razza

    def get_sex(self):
        return self.sex


if __name__ == '__main__':

    opt = ['ins', 'del', 'view', 'exit']
    elenco = set()
    while True:
        print("\n Menu Canile : ")
        choice = input("Cosa Vuoi fare (ins/del/view/exit)?").strip()
        if choice in opt:
            if choice == 'exit':
                print("exit from program...tnx choice = {}".format(choice) + "\n\n\n")
                break
            if choice == 'ins':
                cane = Dog()
                cane.set_name(input("\n Che nome vuoi dare al cane ? \n").strip())
                cane.set_eta(input("\n Eta (anni)? \n").strip())
                cane.set_sex(input("\n Sesso (m/f)? \n").strip())
                print("\n Cane inserito , dati : \n cane nome : {}".format(cane.get_name()))
                print("\n Eta : {}".format(cane.get_eta()))
                print("\n Sesso : {}".format(cane.get_sex()))
                elenco.add(cane)
            if choice == 'view':
                print("\n Totale Cani presenti nel canile : {}".format(len(elenco)))
                for dog in elenco:
                    print("\n -----------------------")
                    print("Nome : {}".format(dog.get_name()))
                    print("Eta : {}".format(dog.get_eta()))
                    print("Sesso : {}".format(dog.get_sex()))
            if choice == "del":
                if(len(elenco) > 0):
                    delcane = input("\n Inserisci il nome del cane da rimuovere : ").strip()
                    index = 0
                    remove_cane = Dog()
                    for cane in elenco:
                        if cane.get_name() == delcane:
                            remove_cane = cane
                        index+1
                    if remove_cane.get_name() != "":
                        elenco.remove(remove_cane)
                        print("\n cane rimosso")
                else:
                    print("\n Attualmente non ci sono cani nel canile")
